/*
hw06, for Ryan Moreida (rmm219) due October 23, 2018
*/

import java.util.Scanner;
public class EncryptedX{
  public static void main(String[] args) {
    
    //declares my scanner
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("What size would you like the square to be? (enter an intiger between 0 and 100):"); //asks the user what size square they want
    
    //checks to make sure the value is an integer
    while(myScanner.hasNextInt() == false){
      myScanner.next();
      System.out.println("Not a valid input, please enter an integer between 0 and 100.");
    }
    
    int squareSize = myScanner.nextInt(); //variable for square size
    
    int spaceCounter = 0; //variable that will be used to find what space in the square is being evaluated
    
    //nested for loop with an if statement inside of the inner for loop to determine the location within the square and determine if a * or a space should go there
    for (int i = 0;i<squareSize; i++){
      for (int j = 0;j<squareSize; j++){
        if(spaceCounter == j || spaceCounter == squareSize-j-1){
          System.out.print(" ");
          
        }
       
        else{System.out.print("*");}
      }
      System.out.println();
    spaceCounter++;
    }
    
    
  }
}
    
    