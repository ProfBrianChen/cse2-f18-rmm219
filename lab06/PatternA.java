import java.util.Scanner;
public class PatternA{
  
  public static void main(String args[]){
    
    Scanner myScanner = new Scanner(System.in);
    
    System.out.println("How many rows do you want in this pattern?");
    
    while(myScanner.hasNextInt()==false){
      myScanner.next();
      System.out.println("Not a valid input.");
    }
    
    int numRows = myScanner.nextInt();
    
    for(int i = 1; i<=numRows;i++)
    {
      for(int j=1; j<=i;j++)
      {
        System.out.print(j+"");
      }
      System.out.println();
      
    }
    
    
  }
}

    