//This program is meant to use recorded data that includes times elapsed and number of wheel turns to produce distance traveled as well as print the time of each trip and number of wheel rotations.
public class Cyclometer{
  //This is the main method recquired for a Java program 
  public static void main(String args[]){
    
    //Inout Data
    int secsTrip1=480;  //time of trip 1
    int secsTrip2=3220; //time of trip 2
    int countsTrip1=1561; //tire rotations trip 1
    int countsTrip2=9037; //tire rotations trip 2
    
    //Intermediate and Output Data
    double wheelDiameter=27.0, pi=3.14159, feetPerMile=5280, inchesperFoot=12, secondsPerMinute=60; //These are all needed constants
    double distanceTrip1, distanceTrip2, totalDistance; //Desired output data
    
    System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+ countsTrip1 + " counts.");
    System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+ countsTrip2 + " counts.");     
    
    distanceTrip1=countsTrip1*wheelDiameter*pi; //trip 1 distance in inches
    distanceTrip1/=inchesperFoot*feetPerMile; //trip 1 distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*pi/inchesperFoot/feetPerMile; //trip 2 in miles                   
    totalDistance=distanceTrip1 + distanceTrip2;
    
    //Print data
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
  }
  
}

                     
                       
                      
