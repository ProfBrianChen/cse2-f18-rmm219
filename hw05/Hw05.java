/*
hw05, for Ryan Moreida (rmm219) due October 9, 2018
*/
import java.util.Scanner; //inputs scanner
public class Hw05{
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner(System.in); //declares my scanner
    
    System.out.println("How many hands would you like to be generated?"); //asks user for number of hands
    
    int numberHands = myScanner.nextInt(); //variable for number of hands
    
    //variables that will be needed to determine probabilities and variables for each card
    int y =0;
    int twoKind = 0;
    int threeKind = 0;
    int fourKind = 0;
    int handCounter = 0;
    int card1 = 0;
    int card2 = 0;
    int card3 = 0;
    int card4 = 0;
    int card5 = 0;
    
    //while loop to generate the cards
    while(numberHands > handCounter){
            card1= (int) (Math.random()*52 + 1);
            card2= (int) (Math.random()*52 + 1);
            card3= (int) (Math.random()*52 + 1);
            card4= (int) (Math.random()*52 + 1);
            card5= (int) (Math.random()*52 + 1);
    
    //determines if any of the randomly selected cards are the same exact card
    if(card1==card2 || card1==card3 || card1==card4 || card1==card5 || card2==card3 || card2==card4
        || card2==card5 || card3==card4 || card3==card5 || card4==card5){
            handCounter--;
        }
    
    //makes all cards be between 1 and 13 (2 through Ace)
    else{
             if(card1 > 13 && card1 < 27){
                    card1 =- 13;
                }
                else if (card1 > 26 && card1 < 40){
                    card1 =- 26;   
                }
                else if (card1 >39 && card1 < 53){
                    card1 =- 39;             
                }
      if(card2 > 13 && card2 < 27){
                    card2 =- 13;
                }
                else if (card2 > 26 && card2 < 40){
                    card2 =- 26;   
                }
                else if (card2 >39 && card2 < 53){
                    card2 =- 39;             
                }
       if(card3 > 13 && card3 < 27){
                    card3 =- 13;
                }
                else if (card3 > 26 && card3 < 40){
                    card3 =- 26;   
                }
                else if (card3 >39 && card3 < 53){
                    card3 =- 39;             
                }
             if(card4 > 13 && card4 < 27){
                    card4 =- 13;
                }
                else if (card4 > 26 && card4 < 40){
                    card4 =- 26;   
                }
                else if (card4 >39 && card4 < 53){
                    card4 =- 39;             
                }
             if(card5 > 13 && card5 < 27){
                    card5 =- 13;
                }
                else if (card5 > 26 && card5 < 40){
                    card5 =- 26;   
                }
                else if (card5 >39 && card5 < 53){
                    card5 =- 39;             
                }
             if(card1==card2){
                 y++;
                }
             if(card1==card3){
                 y++;
                }
             if(card1==card4){
                 y++;
                }
             if(card1==card5){
                 y++;
                }
             if(card2==card3){
                 y++;
                }
             if(card2==card4){
                 y++;
                }
             if(card2==card5){
                 y++;
                }
             if(card3==card4){
                 y++;
                }
             if(card3==card5){
                 y++;
                }
             if(card4==card5){
                 y++;
                }
             if(y==1){
                 twoKind++;
                }
             else if(y==2){
                 threeKind++;
                }
             else if(y==3){
                 fourKind++;
                }
      }
        handCounter++;
        y=0;
    }
      
    double twoKindProb = twoKind/numberHands;
    double threeKindProb = threeKind/numberHands;
    double fourKindProb = fourKind/numberHands;
    
    System.out.println("The number of pairs is "+twoKind+" and the probability of a pair with "+numberHands+" hands is "+((double)(twoKindProb * 1000))/(double)1000);
    System.out.println("The number of three-of-a-kinds is "+threeKind+" and the probability of a three-of-a-kind with "+numberHands+" hands is "+((double)(threeKindProb*1000))/(double)1000);
    System.out.println("The number of four-of-a-kinds is "+fourKind+" and the probability of a four-of-a-kind with "+numberHands+" hands is "+((double)(fourKindProb*1000))/(double)1000);
    
  }
}