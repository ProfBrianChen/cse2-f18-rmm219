/* 
hw03, Program 2 for Ryan Moreida (rmm219) due September 18, 2018
*/

import java.util.Scanner;
public class Pyramid{
public static void main(String[] args) {
    
         Scanner myScanner = new Scanner( System.in );  //importing and defining the scanner
            
         System.out.print("The square side of the pyramid is (input length): "); 
         double squareSide = myScanner.nextDouble(); //variable for the length of the square side of the pyramid
         double squareSidesquared = Math.pow(squareSide, 2);
    
         System.out.print("The height of the pyramid is (input height): ");
         double Height = myScanner.nextDouble(); //variable for the height of the pyramid
           
         double Volume; //introducing the volume variable
         
         Volume = squareSidesquared * Height / 3; //defining the volume variable
           
         System.out.println("The volume of the pyramid is "+Volume+"."); //printing the volume of the pyramid given the entered height and length 
           
  }
}

        

    
    