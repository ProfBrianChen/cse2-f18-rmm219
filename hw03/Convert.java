/* 
hw03, Program 1 for Ryan Moreida (rmm219) due September 18, 2018
*/

import java.util.Scanner;
public class Convert{
public static void main(String[] args) {
    
        Scanner myScanner = new Scanner( System.in ); //importing and defining the scanner
    
        System.out.print("Enter the affected area in acres: ");
    
        double acresAffected = myScanner.nextDouble(); //area affected in acres
    
        System.out.print("Enter the amount of rainfall in inches: ");
    
        double inchesRainfall = myScanner.nextDouble(); //amount of rainfall in inches 

        double milesAffected;//cubic miles of rainfall
          
        double gallonsAcreinch = 27154.3;//gallons needed to cover an acre with one inch of water
          
        double gallonsCubicmiles = 1101117130711.3; //gallons to cubic miles
          
        milesAffected = acresAffected * inchesRainfall * gallonsAcreinch / gallonsCubicmiles; //conversion from acre-inches to gallons to cubic miles
          
        System.out.println("The total amount of rain was "+milesAffected+" cubic miles."); //printing the amount of rainfall in cubic miles
          
  }
}

          
        


