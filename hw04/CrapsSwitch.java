/*
hw04, program 2 for Ryan Moreida (rmm219) due September 25, 2018
*/

import java.util.Scanner;
public class CrapsSwitch{
  public static void main(String[] args) {
  
  Scanner myScanner = new Scanner( System.in ); //importing and defining the scanner
    
  System.out.println("Would you like randomly cast dice? (Answer Yes or No) "); //prompts the user to determine if they want randomly selected dice values
    
  String userChoice = myScanner.next(); //users choice of die
  
  //variables for each dice
  int dice1 = 0;
  int dice2 = 0;
  
  //switch statement that as the user if they want randomly selected dice values and checks dice values if manually inputed  
  switch (userChoice){
    case "Yes":
    dice1 = (int) (Math.random()*6)+1;
    dice2 = (int) (Math.random()*6)+1;
    System.out.println(""+dice1+" is your first dice and "+dice2+" is your second dice");
    break;   
    case "No":
    System.out.println("Choose a number between 1 and 6 for dice 1");
    dice1= myScanner.nextInt();
    if(dice1 > 6 || dice1 < 1){
      System.out.println("Number not in range");
    }
    System.out.println("Choose a number between 1 and 6 for dice 2");
    dice2= myScanner.nextInt();
    if(dice2 > 6 || dice2 < 1){
      System.out.println("Number not in range");
    break;
     }
  }
    
//nested switch statements to determine slang for the rolls of the die
   switch (dice1){
   case 1:
     switch (dice2){
     case 1:  System.out.println("Snake Eyes");
     break;
     case 2:  System.out.println("Ace Duece");
     break;
     case 3:  System.out.println("Easy Four") ;
     break;
     case 4:  System.out.println("Fever Five");
     break;
     case 5:  System.out.println("Easy Six");
     break;
     case 6:  System.out.println("Seven Out"); 
     }
     break;  
   case 2:
     switch (dice2){
     case 1:  System.out.println("Ace Duece");
     break;
     case 2:  System.out.println("Hard Four");
     break;
     case 3:  System.out.println("Fever Five") ;
     break;
     case 4:  System.out.println("Easy Six");
     break;
     case 5:  System.out.println("Seven Out");
     break;
     case 6:  System.out.println("Easy Eight"); 
     }    
     break;  
   case 3:
     switch (dice2){
     case 1:  System.out.println("Easy Four");
     break;
     case 2:  System.out.println("Fever Five");
     break;
     case 3:  System.out.println("Hard Six") ;
     break;
     case 4:  System.out.println("Seven Out");
     break;
     case 5:  System.out.println("Easy Eight");
     break;
     case 6:  System.out.println("Nine"); 
     }   
     break;  
   case 4:
     switch (dice2){
     case 1:  System.out.println("Fever Five");
     break;
     case 2:  System.out.println("Easy Six");
     break;
     case 3:  System.out.println("Seven Out") ;
     break;
     case 4:  System.out.println("Hard Eight");
     break;
     case 5:  System.out.println("Nine");
     break;
     case 6:  System.out.println("Easy Ten"); 
     } 
     break;  
   case 5:
     switch (dice2){
     case 1:  System.out.println("Easy Six");
     break;
     case 2:  System.out.println("Seven Out");
     break;
     case 3:  System.out.println("Easy Eight") ;
     break;
     case 4:  System.out.println("Nine");
     break;
     case 5:  System.out.println("Hard Ten");
     break;
     case 6:  System.out.println("Yo-leven"); 
     }  
     break;  
   case 6:
     switch (dice2){
     case 1:  System.out.println("Seven Out");
     break;
     case 2:  System.out.println("Easy Eight");
     break;
     case 3:  System.out.println("Nine") ;
     break;
     case 4:  System.out.println("Easy Ten");
     break;
     case 5:  System.out.println("Yo-leven");
     break;
     case 6:  System.out.println("Boxcars"); 
     }   
     break;  
   }   

  }
}
       
    
   
    
  
  
