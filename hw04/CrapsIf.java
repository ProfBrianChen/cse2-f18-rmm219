/*
hw04, program 1 for Ryan Moreida (rmm219) due September 25, 2018
*/

import java.util.Scanner;
public class CrapsIf{
  public static void main(String[] args) {

  Scanner myScanner = new Scanner( System.in ); //importing and defining the scanner
    
  System.out.println("Would you like randomly cast dice? (Answer Yes or No) "); //prompts the user to determine if they want randomly selected dice values
    
  String userChoice = myScanner.next(); //users choice of die
  
  //variables for each dice 
  int dice1 = 0;
  int dice2 = 0;
   
  //if statement to determine if the user wants randomly selected dice values and checks dice values if manually inputed   
  if (userChoice.equalsIgnoreCase("Yes")){
    dice1 = (int) (Math.random()*6)+1;
    dice2 = (int) (Math.random()*6)+1;
    System.out.println(""+dice1+" is your first dice and "+dice2+" is your second dice");
  }
  else{
    System.out.println("Choose a number between 1 and 6 for dice 1");
    dice1= myScanner.nextInt();
    if(dice1 > 6 || dice1 < 1){
      System.out.println("Number not in range");
      
    }
    System.out.println("Choose a number between 1 and 6 for dice 2");
    dice2= myScanner.nextInt();
    if(dice2 > 6 || dice2 < 1){
      System.out.println("Number not in range");
 
    }
  }
    
//if statements to determine slang for the rolls of the die
   if(dice1==1&&dice2==1){
     System.out.println("Snake Eyes");
   }
   else if(dice1==1&&dice2==2 || dice2==2&&dice2==1){
     System.out.println("Ace Duece");
   }   
   else if(dice1==1&&dice2==3 || dice2==3&&dice2==1){
     System.out.println("Easy Four");
   }   
   else if(dice1==1&&dice2==4 || dice2==4&&dice2==1){
     System.out.println("Fever Five");
   }   
   else if(dice1==1&&dice2==5 || dice2==5&&dice2==1){
     System.out.println("Easy Six");
   }   
   else if(dice1==1&&dice2==6 || dice2==6&&dice2==1){
     System.out.println("Seven Out");
   }   
   else if(dice1==2&&dice2==2){
     System.out.println("Hard Four");
   }   
   else if(dice1==2&&dice2==3 || dice2==3&&dice2==2){
     System.out.println("Fever Five");
   }    
   else if(dice1==2&&dice2==4 || dice2==4&&dice2==2){
     System.out.println("Easy Six");
   }
   else if(dice1==2&&dice2==5 || dice2==5&&dice2==2){
     System.out.println("Seven Out");
   } 
   else if(dice1==2&&dice2==6 || dice2==6&&dice2==2){
     System.out.println("Easy Eight");
   } 
   else if(dice1==3&&dice2==3){
     System.out.println("Hard Six");
   } 
   else if(dice1==3&&dice2==4 || dice2==4&&dice2==3){
     System.out.println("Seven Out");
   } 
   else if(dice1==3&&dice2==5 || dice2==5&&dice2==3){
     System.out.println("Easy Eight");
   } 
   else if(dice1==3&&dice2==6 || dice2==6&&dice2==3){
     System.out.println("Nine");
   } 
   else if(dice1==4&&dice2==4){
     System.out.println("Hard Eight");
   } 
   else if(dice1==4&&dice2==5 || dice2==5&&dice2==4){
     System.out.println("Nine");
   } 
   else if(dice1==4&&dice2==6 || dice2==6&&dice2==4){
     System.out.println("Easy Ten");
   } 
   else if(dice1==5&&dice2==5){
     System.out.println("Hard Ten");
   } 
   else if(dice1==5&&dice2==6 || dice2==6&&dice2==5){
     System.out.println("Yo-leven");
   }
   else if(dice1==6&&dice2==6){
     System.out.println("Boxcars");
   }
  }
}
    