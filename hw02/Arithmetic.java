/* 
hw02 for Ryan Moreida (rmm219) due September 11, 2018
*/

public class Arithmetic{
  
  public static void main(String args[]){
    
    int numPants = 3; //number of pants
    double pantsPrice = 34.98; //cost per pair of pants
    int numShirts = 2; //number of sweatshirts
    double shirtPrice = 24.99; //cost per shirt
    int numBelts = 1; //number of belts
    double beltPrice = 33.99; //cost per belts
    double paSalesTax = .06; //tax rate
    
    double totalCostOfPants; //total pants cost
    double totalCostOfShirts; //total shirts cost
    double totalCostOfBelts; //total belts cost
    
   double salesTaxForPants , salesTaxForShirts , salesTaxForBelts; //sales tax for buying all of each kind of item
   double TotalPreTaxCost; //total pre-tax cost
   double TotalSalesTax; //total sales tax
   double TotalCost; //total cost with tax
   
   //total costs for the various items  
   totalCostOfPants = numPants * pantsPrice;
   totalCostOfShirts = numShirts * shirtPrice;
   totalCostOfBelts = numBelts * beltPrice;
   
   //sales tax for the various items 
   salesTaxForPants = numPants * pantsPrice * paSalesTax;
   salesTaxForShirts = numShirts * shirtPrice * paSalesTax;
   salesTaxForBelts = numBelts * beltPrice * paSalesTax;
     
   TotalPreTaxCost = totalCostOfPants + totalCostOfShirts + totalCostOfBelts;
   TotalSalesTax = salesTaxForPants + salesTaxForShirts + salesTaxForBelts;
   
   TotalCost = TotalPreTaxCost + TotalSalesTax;
    
   System.out.println("The three pairs of pants cost $"+totalCostOfPants +" and have a sales tax of $"+((int)(salesTaxForPants * 100))/(double)100 +".");
   System.out.println("The two pairs of shirts cost $"+totalCostOfShirts+" and have a sales tax of $"+((int)(salesTaxForShirts * 100))/(double)100 +".");
   System.out.println("The one belt cost $"+totalCostOfBelts+" and has a sales tax of $"+((int)(salesTaxForBelts * 100)) /(double)100 +".");
     
   System.out.println("The total cost of the goods before taxes is $"+TotalPreTaxCost+".");
   System.out.println("The sales tax associated with the purchase of all of the goods is $"+((int)(TotalSalesTax * 100))/ (double)100 +".");
   System.out.println("The total cost of the purchase with sales tax is $"+((int)(TotalCost * 100))/(double)100 +".");
  
  }
}

     