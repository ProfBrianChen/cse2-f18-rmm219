public class WelcomeClass{
  
  public static void main(String args[]){
    
  System.out.println("   -----------"); 
  System.out.println("   | WELCOME |");
  System.out.println("   -----------");
  System.out.println("   ^  ^  ^  ^  ^  ^ ");
  System.out.println("  / \\/ \\/ \\/ \\/ \\/ \\"); 
  System.out.println("<-R--M--M--2--1--9->");
  System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
  System.out.println("  v  v  v  v  v  v");
  System.out.println();
  System.out.println("I am currently a senior pursuing degrees in Applied Mathematics and Finance."); 
  System.out.println("During my free time I enjoy playing golf.");                   
  
   }
                     
}
                   