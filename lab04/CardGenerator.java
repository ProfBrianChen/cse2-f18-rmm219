public class CardGenerator{
  public static void main(String[] args) {
    
  int cardValue; //variable for card value

  int valueReduced; //card value after suit is determined

  int max = 52;

  int min = 1;

  int range = max - min +1;

  String suit; //string for suit

  String identity = "Test";
  
  cardValue = (int) (Math.random()*range) + min;
  //if statements used to assign suit value 
    
  if (cardValue < 14){
    suit = "Clubs";
    valueReduced = cardValue;
  }
  else if (cardValue < 27){
    suit = "Diamonds";
    valueReduced = cardValue -13;   
  }
  else if (cardValue < 40){
    suit = "Spades";
    valueReduced = cardValue - 26;
  }
  else {
    suit = "Hearts";
    valueReduced = cardValue - 39;
  }
 
 //switch statement used to determine card value   
    
  switch (valueReduced){
    case 1: identity = "Ace";
  break;
    case 2: identity = "2";
  break;
    case 3: identity = "3";
  break;
    case 4: identity = "4";
  break;
    case 5: identity = "5";
  break;
    case 6: identity = "6";
  break;
    case 7: identity = "7";
  break;
    case 8: identity = "8";
  break;
    case 9: identity = "9";
  break;
    case 10: identity = "10";
  break;
    case 11: identity = "Jack";
  break;
    case 12: identity = "Queen";
  break;
    case 13: identity = "King";
  break;
}
  
  System.out.println("You picked the "+ identity +" of "+ suit +"."); //prints the value and suit of the card
  
}
}

    
  